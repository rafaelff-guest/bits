Title: 데비안 공식 채널
Slug: official-communication-channels
Date: 2020-03-16 14:00
Author: Laura Arjona Reina, Ana Guerrero Lopez and Donald Norwood
Tags: project, announcement
Lang: ko
Translator: Sebul
Status: published

때때로, 우리는 데비안에서 우리의 공식적인 의사소통 경로에 대한 질문과 
비슷한 이름의 웹사이트를 소유할 수 있는 데비안의 상태에 대한 질문을 받습니다.


데비안 웹사이트 [www.debian.org](https://www.debian.org) 는
주요 소통 수단입니다. 커뮤니티의 현재 이벤트 및 개발 진행 상황에 대한 정보를 원하는 사람들은 
데비안 웹사이트의 [데비안 뉴스](https://www.debian.org/News/) 섹션에 관심이 있을 수 있습니다.

덜 공식적인 발표는 데비안의 공식 데비안 블로그 [Bits from Debian](https://bits.debian.org)와 
더 짧은 뉴스 항목을 위한 [Debian micronews](https://micronews.debian.org)가 있습니다.


공식 뉴스레터인 [Debian Project News](https://www.debian.org/News/weekly/) 및 
모든 뉴스 또는 프로젝트 변경에 대한 공식 발표는 저희 웹사이트에 이중으로 게시되어 있으며 
공식 메일링 리스트 [debian-announce](https://lists.debian.org/debian-announce/) 또는
[debian-news](https://lists.debian.org/debian-news/)에 게시되어 있습니다.
이들 메일링 리스트에 쓰는 건 제한됩니다.


우리는 또한 데비안 프로젝트, 즉 데비안이 어떻게 구성되어 있는지 발표할 기회를 갖기 바랍니다.
간단히, 데비안은 구조적입니다.



데비안은 우리 [헌법](https://www.debian.org/devel/constitution)에 의해 규제되는 구조를 가지고 있습니다. 
담당자와 위임된 구성원은 [조직 구조](https://www.debian.org/intro/organization) 페이지에 나열되어 있습니다. 
추가 팀이 [팀](https://wiki.debian.org/Teams) 페이지에 나열됩니다.


공식 데비안 멤버의 전체 목록은 회원이 관리되는 [새 회원 페이지](https://nm.debian.org/members)에서 확인할 수 있습니다.
데비안 기고자의 더 넓은 목록은 [기고자](https://contributors.debian.org) 페이지에서 찾을 수 있습니다.


물어볼 게 있으면, 
[press@debian.org](mailto:press@debian.org)에 있는 press 팀에 들르세요.
