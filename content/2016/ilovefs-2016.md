Title: I love Free Software Day 2016: Show your love for Free Software
Slug: ilovefs-2016
Date: 2016-02-14 01:10
Author: Laura Arjona Reina
Tags: contributing, debian, free software, FSFE
Status: published

<!---
note for translators: try a localized banner, e.g. https://fsfe.org/campaigns/ilovefs/artwork/graphics/ilovefs-banner-medium-fr.png
-->

[![ILoveFS banner](|static|/images/ilovefs-banner-medium-en.png)][ilovefs-link]

Today February 14th, the Free Software Foundation Europe (FSFE) celebrates
the  ["I Love Free Software" day][ilovefs-link]. I Love Free Software day is a day for Free
Software users to appreciate and thank the contributors of their favourite software
applications, projects and organisations.

We take this opportunity to say "thank you" to all the Debian upstreams and downstreams,
and all the Debian developers and contributors. Thanks for your work and dedication to
free software!

There are many ways to participate in this ILoveFS day and we encourage everybody
to join in and celebrate. Show your love to Debian developers, contributors and
teams virtually on social networks using the #ilovefs hashtag and spreading the
word in your own social media circles, or by visiting the [ILoveFS](http://ilovefs.org) campaign
website to find and use some of the promotional materials available such as
postcards and banners.

To learn more about the FSFE, you can read their
[announcement of this campaign][announcement-link]
or visit their [general website][fsfe-link].

[ilovefs-link]: https://fsfe.org/campaigns/ilovefs/2016/
[announcement-link]: https://fsfe.org/news/2016/news-20160208-01.en.html
[fsfe-link]: https://fsfe.org
