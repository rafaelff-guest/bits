Title: Elecciones del Debian Project Leader del 2016
Date: 2016-03-27 20:55
Tags: dpl, vote
Slug: dpl-elections-2016
Author: Ana Guerrero Lopez
Lang: es
Translator: Adrià García-Alzórriz
Status: draft

Es esta época del año de nuevo para el Proyecto Debian: ¡[las elecciones
para el líder del Proyectol](https://www.debian.org/vote/2016/vote_001)!

Neil McGovern, que ha estado ostentando el cargo durante el último año
no ha buscado la reelección. Los desarrolladores Debian tendrán que 
elegir votando el único candidato que se ha presentado, 
[Mehdi Dogguy](https://www.debian.org/vote/2016/platforms/mehdi)
o *Ninguno de los anteriores*. Si ganase las elecciones *Ninguno de los 
anteriores*, entonces las votaciones se repetirían tantas veces como 
fuera necesario.

Mehdi Dogguy fue un candidato al cargo de DPL el pasado año, quedando
segundo con un número de votos muy cercando al del ganador Neil McGovern.

Estamos en medio del periodo de campaña que finalizará el próximo 2 de 
abril. El candidato y los colaboradores de Debian esperan involucrarse 
en debates y discusiones en la [lista de correo debian-vote mailing list](http://lists.debian.org/debian-vote/).

El periodo de votaciones empezará el próximo 3 de abril y estará 
abierto durante las dos próximas semanas. Los desarrolladores de Debian
votarán la persona que querrán que guíe el proyecto durante el próximo
año. El resultado se publicará el 17 de abril, momento en el que 
empezará el mandato del nuevo líder del proyecto.
