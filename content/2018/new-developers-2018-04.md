Title: New Debian Developers and Maintainers (March and April 2018)
Slug: new-developers-2018-04
Date: 2018-05-02 22:03
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Translator:
Status: published


The following contributors got their Debian Developer accounts in the last two months:

  * Andreas Boll (aboll)
  * Dominik George (natureshadow)
  * Julien Puydt (jpuydt)
  * Sergio Durigan Junior (sergiodj)
  * Robie Basak (rbasak)
  * Elena Grandi (valhalla)
  * Peter Pentchev (roam)
  * Samuel Henrique (samueloph)

The following contributors were added as Debian Maintainers in the last two months:

  * Andy Li
  * Alexandre Rossi
  * David Mohammed
  * Tim Lunn
  * Rebecca Natalie Palmer
  * Andrea Bolognani
  * Toke Høiland-Jørgensen
  * Gabriel F. T. Gomes
  * Bjorn Anders Dolk
  * Geoffroy Youri Berret
  * Dmitry Eremin-Solenikov

Congratulations!




