Title: 25 tuổi và dấu mốc
Slug: debian-is-25
Date: 2018-08-16 8:50
Author: Ana Guerrero Lopez
Artist: Angelo Rosa
Tags: debian, birthday
Translator: Trần Ngọc Quân
Lang: vi
Status: published

![Debian 25 tuổi được tạo bởi Angelo Rosa](|static|/images/debian25years.png)

Khi Ian Murdock công bố 25 năm trước tại comp.os.linux.development,
*"the imminent completion of a brand-new Linux release, [...] the Debian Linux Release"*,
không ai có thể ngờ rằng "Debian Linux Release" trở thành cái mà hôm nay
được biết đến là Dự án Debian, một trong những dự án phần mềm tự do lớn nhất và có tầm ảnh hưởng nhất.
Sản phẩm chính của nó là Debian, một hệ điều hành (OS) tự do dành cho máy tính của bạn, cũng như là
rất nhiều các hệ thống khác những thứ mà nâng cao đời sống của bạn. Từ xử lý những công việc bên trong sân bay
ở quanh bạn cho đến hệ thống giải trí trên xe hơi của bạn, và từ máy chủ đám mây phục vụ trang thông tin điện tử
yêu thích của bạn đến các thiết bị IoT cái mà giao tiếp với chúng, Debian có thể phục vụ tất.

Ngày nay, dự án Debian là một tổ chức rộng lớn và thịnh vượng với một số lượng không thể đếm nổi
các nhóm tự-tổ-chức được tổ hợp từ những người tình nguyện. Trong khi nhìn từ bên ngoài nó thường lộn xộn, dự án
được duy trì liên tục bằng hai tài liệu có tính nền tảng chính của nó: [Khế ước xã hội Debian], cái mà cung cấp
tầm nhìn về cải tiến xã hội, và [Nguyên tắc phần mềm tự do Debian], cái mà cung cấp một chỉ dẫn
để quyết định xem phần mềm nào được coi là dùng được. Chúng được bổ xung bởi [Constitution] của dự án cái mà
đề ra cấu trúc dự án, và [Quy tắc ứng xử], cái mà đặt quy tắc để tương tác bên trong dự án.

Mỗi ngày trong suốt 25 năm qua, mọi người gửi báo cáo lỗi và các miếng vá, tải
lên các gói, cập nhật các bản dịch, tạo tranh ảnh nghệ thuật, tổ chức các sự kiện giới thiệu về Debian, cập nhật
trang thông tin điện tử, dạy những người khác cách dùng Debian, và tạo hàng trăm bản dẫn xuất.

**Giờ là 25 năm kế tiếp - và hy vọng rằng còn nhiều lần như thế nữa!**


[Khế ước xã hội Debian]: https://www.debian.org/social_contract
[Nguyên tắc phần mềm tự do Debian]: https://www.debian.org/social_contract#guidelines
[Constitution]: https://www.debian.org/devel/constitution
[Quy tắc ứng xử]: https://www.debian.org/code_of_conduct
