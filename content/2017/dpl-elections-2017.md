Title: Debian Project Leader elections 2017
Date: 2017-03-25 22:30
Tags: dpl, vote
Slug: dpl-elections-2017
Author: Laura Arjona Reina
Status: published

It's that time of year again for the Debian Project: [the elections of
its Project Leader](https://www.debian.org/vote/2017/vote_001)!

The Project Leader position is described in
the [Debian Constitution](https://www.debian.org/devel/constitution#item-5).

Two Debian Developers run this year to become Project Leader:
[Mehdi Dogguy](https://www.debian.org/vote/2017/platforms/mehdi),
who has held the office for the last year,
and [Chris Lamb](https://www.debian.org/vote/2017/platforms/lamby).

We are in the middle of the campaigning period that will last until
the end of April 1st. The candidates and Debian contributors
are already engaging in debates and discussions on
the [debian-vote mailing list](http://lists.debian.org/debian-vote/).

The voting period starts on April 2nd, and during the following two
weeks, Debian Developers can vote to choose the person
that will fit that role for one year.

The results will be published on April 16th
with the term for new the project leader starting the following day.
