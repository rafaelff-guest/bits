Title: New Debian Developers and Maintainers (March and April 2017)
Slug: new-developers-2017-04
Date: 2017-05-15 12:30
Author: Jean-Pierre Giraud
Tags: project
Status: published

The following contributors got their Debian Developer accounts in the last two months:

  * Guilhem Moulin (guilhem)
  * Lisa Baron (jeffity)
  * Punit Agrawal (punit)

The following contributors were added as Debian Maintainers in the last two months:

  * Sebastien Jodogne
  * Félix Lechner
  * Uli Scholler
  * Aurélien Couderc
  * Ondřej Kobližek
  * Patricio Paez

Congratulations!

