Title: Debian 9.0 Stretch is uitgebracht!
Date: 2017-06-18 08:25
Tags: stretch
Slug: stretch-released
Author: Ana Guerrero Lopez and Laura Arjona Reina
Status: published
Lang: nl
Translator: Frans Spiesschaert


![Alt Stretch werd uitgebracht](|static|/images/banner_stretch.png)

Laat u door de purperen speelgoedoctopus omarmen! We zijn verheugd
de uitgave van Debian 9.0, codenaam *Stretch* aan te kondigen.

**Wilt u het installeren?**
Kies uw favoriete [installatiemedium](https://www.debian.org/distrib/)
uit blu-raydisc, DVD,
CD of USB-stick. Lees vervolgens de
[installatiehandleiding](https://www.debian.org/releases/stretch/installmanual).

**Bent u reeds een tevreden gebruiker van Debian en wilt u enkel
opwaarderen?**
U kunt gemakkelijk een opwaardering van uw huidige Debian 8 Jessie
installatie uitvoeren.
Lees in dat verband de [notities bij de
release](https://www.debian.org/releases/stretch/releasenotes).

**Wilt u de uitgave vieren?**
Deel dan de [banner van deze
blog](https://wiki.debian.org/DebianArt/Themes/softWaves?action=AttachFile&do=view&target=wikiBanner.png) in uw blog of uw website!
