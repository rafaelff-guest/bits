Title: Build Android apps with Debian: apt install android-sdk
Slug: build-android-apps-with-debian
Date: 2017-03-15 12:00
Author: Hans-Christoph Steiner and Kai-Chung Yan (殷啟聰)
Tags: announce, android
Status: published

In Debian stretch, the upcoming new release, it is now possible to build Android
apps using only packages from Debian.  This will provide all of the tools needed
to build an Android app targeting the "platform" `android-23` using the SDK
`build-tools` 24.0.0.  Those two are the only versions of "platform" and
"build-tools" currently in Debian, but it is possible to use the Google binaries
by installing them into `/usr/lib/android-sdk`.

This doesn't cover yet all of the libraries that are used in the app, like the
Android Support libraries, or all of the other myriad libraries that are usually
fetched from jCenter or Maven Central.  One big question for us is whether and
how libraries should be included in Debian.  All the Java libraries in Debian
can be used in an Android app, but including something like Android Support in
Debian would be strange since they are only useful in an Android app, never for
a Debian app.


## Building apps with these packages

Here are the steps for building Android apps using Debian's Android SDK on
Stretch.

 1. `sudo apt install android-sdk android-sdk-platform-23`
 2. `export ANDROID_HOME=/usr/lib/android-sdk`
 3. In _build.gradle_, set _compileSdkVersion_ to 23 and _buildToolsVersion_ to
24.0.0
 4. run `gradle build`

The Gradle Android Plugin is also packaged. Using the Debian package instead of
the one from online Maven repositories requires a little configuration before
running _gradle_. In the _buildscript_ block:

 * add `maven { url 'file:///usr/share/maven-repo' }` to repositories
 * use `compile 'com.android.tools.build:gradle:debian'` to load the plugin

Currently there is only the target platform of API Level 23 packaged, so only
apps targeted at _android-23_ can be built with only Debian packages. There are
plans to add more API platform packages via backports. Only _build-tools_ 24.0.0
is available, so in order to use the SDK, build scripts need to be modified.
Beware that the Lint in this version of Gradle Android Plugin is still
problematic, so running the :lint tasks might not work. They can be turned off
with _lintOptions.abortOnError_ in _build.gradle_. Google binaries can be
combined with the Debian packages, for example to use a different version of the
platform or build-tools.


## Why include the Android SDK in Debian?

While Android developers could develop and ship apps right now using these
Debian packages, this is not very flexible since only `build-tools-24.0.0` and
`android-23` platform are available.  Currently, the [Debian Android Tools
Team](https://wiki.debian.org/AndroidTools) is not aiming to cover the most
common use cases. Those are pretty well
covered by Google's binaries (except for the proprietary license on the Google
binaries), and are probably the most work for the Android Tools Team to cover.
The current focus is on use cases that are poorly covered by the Google
binaries, for example, like where only specific parts of the whole SDK are used.
 Here are some examples:

 * tools for security researchers, forensics, reverse engineering, etc. which
can then be included in live CDs and distros like Kali Linux
 * a hardened APK signing server using _apksigner_ that uses a standard,
audited, public configuration of all reproducibly built packages
 * Replicant is a 100% free software Android distribution, so of course
 [they want to have a 100% free software SDK](http://blog.replicant.us/2017/02/replicant-6-0-development-updates/)
 * high security apps need a build environment that matches their level of
security, the Debian Android Tools packages are [reproducibly built](https://reproducible-builds.org)
only from publicly available sources
 * support architectures besides i386 and amd64, for example, the Linaro LAVA
setup for testing ARM devices of all kinds uses the adb packages on ARM servers
to make their whole testing setup all ARM architecture
 * dead simple install with strong trust path with mirrors all over the world

In the long run, the Android Tools Team aims to cover more use cases well, and
also building the Android NDK. This all will happen more quickly if there are
more contributors on the Android Tools team!  Android is the most popular mobile
OS, and can be 100% free software like Debian.  Debian and its derivatives are
one of the most popular platforms for Android development.  This is an important
combination that should grow only more integrated.

Last but not least, the Android Tools Team wants feedback on how this should all
work, for example, ideas for how to nicely integrate Debian's Java libraries
into the Android _gradle_ workflow. And ideally, the Android Support libraries
would also be reproducibly built and packaged somewhere that enforces only free
software.  Come find us on IRC and/or email!
[https://wiki.debian.org/AndroidTools#Communication_Channels](https://wiki.debian.org/AndroidTools#Communication_Channels)
