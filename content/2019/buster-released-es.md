Title: ¡Se ha publicado Debian 10 «buster»!
Date: 2019-07-07 03:25
Tags: buster
Slug: buster-released
Author: Ana Guerrero Lopez, Laura Arjona Reina y Jean-Pierre Giraud
Artist: Alex Makas
Lang: es
Translator: Laura Arjona Reina
Status: published

[![Alt Se ha publicado buster](|static|/images/upcoming-buster.png)](https://deb.li/buster)

¿Has soñado siempre con una mascota leal? ¡Está aquí, y su nombre es Buster!
Nos alegra anunciar la nueva versión Debian 10, con nombre en clave *buster*.


**¿Quieres instalarlo?**
Elige tu [modo de instalación](https://www.debian.org/distrib/) preferido
y lee la [guía de instalación](https://www.debian.org/releases/buster/installmanual.es.html).
También puedes utilizar una imagen oficial para la nube directamente desde tu proveedor de nube,
o probar Debian antes de instalarlo usando nuestras imágenes «en vivo».

**¿Eres ya un usuario feliz de Debian y solo quieres actualizarte?**
Simplemente puedes actualizar desde tu Debian 9 «stretch»;
por favor lee antes las [notas de publicación](https://www.debian.org/releases/buster/releasenotes.es.html).


**¿Quieres celebrar la publicación de «buster»?**
Proporcionamos [grafismo sobre buster](https://wiki.debian.org/DebianArt/Themes/futurePrototype) que puedes
compartir o usar como base de tus propias creaciones. ¡Sigue la conversación sobre buster en las redes sociales
a traves de las etiquetas «hashtag» #ReleasingDebianBuster
y #Debian10Buster o súmate en persona o a través de internet a una [Fiesta de publicación](https://wiki.debian.org/ReleasePartyBuster)!
