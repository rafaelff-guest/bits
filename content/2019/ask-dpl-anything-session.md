Title: Introduction to the new DPL, ask him anything! 
Date: 2019-04-26 19:20
Tags: dpl,debian-meeting
Slug: ask-dpl-anything
Author: Jonathan Carter
Status: published

We have a new DPL! On 21 April 2019 Sam Hartman started his term as Debian Project Leader.

Join us on the [#debian-meeting](https://webchat.oftc.net/?channels=#debian-meeting) channel 
on the [OFTC](https://www.oftc.net/) IRC network
on 10 May 2019 at 10:00 UTC for an introduction to our new DPL,
and also to have the chance to ask him any questions that you may have.

Your IRC nick needs to be registered in order to join the channel. Refer to the
[Register your account](https://www.oftc.net/Services/) section on the oftc website
for more information on how to register your nick.

We plan to have many more project-wide IRC sessions in the future.

You can always refer to 
the [debian-meeting wiki page](https://wiki.debian.org/IRC/debian-meeting)
for the latest information and up to date schedule.
