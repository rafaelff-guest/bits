Title: Call for Debian projects and mentors in the GSoC 2013
Date: 2013-03-23 00:06
Tags: announce, gsoc
Slug: call-projects-mentors-gsoc-2013
Author: Ana Guerrero
Status: published

![Alt GSoC 2013 banner](|static|/images/gsoc-2013-logo.png)

As we did in previous years, the Debian Project is applying to become a mentoring organization during the 2013 edition of the Google Summer of Code program. We're now looking for projects and mentors. If you have an idea for a project, please publish it on [the wiki page](http://wiki.debian.org/SummerOfCode2013), filling out the template, and send us an email on [the coordination mailing-list](http://lists.alioth.debian.org/cgi-bin/mailman/listinfo/soc-coordination).


[Google Summer of Code](http://code.google.com/soc/) is a program that allows students to work over the summer on free software projects, paid supported financially by Google. In order to be accepted as a mentoring organization, Debian needs to present a good list of projects to be proposed to the students.


If you need help with an idea in drafting a project proposal, or on anything else related to GSoC, feel free to contact us by email at [the coordination mailing-list](http://lists.alioth.debian.org/cgi-bin/mailman/listinfo/soc-coordination), or on our IRC channel #debian-soc (on irc.debian.org). You can also [browse the list of project with confirmed mentors](http://wiki.debian.org/SummerOfCode2013/Projects#Projects_with_confirmed_mentors) for inspiration.

This post is a brief version of [this email from GSoC Admins in Debian](https://lists.debian.org/debian-devel-announce/2013/02/msg00007.html)
