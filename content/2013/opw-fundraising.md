Title: Last days to donate to Debian Outreach Program for Women
Date: 2013-11-10
Tags: opw
Slug: opw-fundraising
Author: Brian Gupta
Status: published

The matching program to help fund Debian participation in [OPW](http://wiki.debian.org/OPW) will end in 6 days (November 14th).
We need your help, and still have a ways to go to reach our funding targets.
Please consider [donating](http://debian.ch/opw2013) today!


The rules are simple:

* for each dollar donated by an individual to Debian through the mechanism, a sponsor will donate another dollar to Debian's OPW program;
* individual donation in USD will be matched only up to 200 USD each;
* only donations in USD will be matched;
* the sponsor will match the donated funds up to a maximum total of 2500 USD;
* all money raised through this program will be earmarked for OPW participation, either in 2013 or in the future;
* this program will be in place until Nov 14th, so please act quickly!


If you'd like to participate as intern, the application deadline is
very near (November 11th). You can find [here](http://wiki.debian.org/OPW) more info on how to apply.

