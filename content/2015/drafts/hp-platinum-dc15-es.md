Title: Patrocinio Platino de Hewlett-Packard para la DebConf15
Slug: hp-platinum-debconf15
Date: 2015-03-25 15:00
Author: Laura Arjona Reina
Translator: Daniel Cialdella
Tags: debconf15, debconf, sponsors, HP
Lang: es
Status: draft

[![HPlogo](|static|/images/logo_hp.png)](http://www.hp.com/)

Estamos muy contentos en anunciar que [**HP**](http://www.hp.com/) ha 
confirmado su apoyo para la DebConf15 como **Patrocinador de Platino**.

"*El grupo hLinux se alegra de continuar con la larga tradición de HP de 
apoyar a Debian y DebConf*," dijo Steve Geary, Director 
Senior en Hewlett-Packard. 

Hewlett-Packard es una de las más grandes empresas en el mundo, provee 
un gran rango de productos y servicios como servidores, ordenadores, 
impresoras, productos de almacenamiento, programas, soluciones de 
cómputo en la nube, etc.

Hewlett-Packard ha sido socio de desarrollo de Debian desde hace mucho tiempo, 
y provee de hardware para el desarrollo de las 
adaptaciones («ports»), servidores de réplica de Debian, y otros servicios Debian (las 
donaciones de equipos HP están indicadas en la página [Máquinas en 
Debian](https://db.debian.org/machines.cgi) ).

Con este compromiso adicional como Patrocinador de Platino, HP contribuye a 
hacer posible nuestra conferencia anual y apoya directamente el 
progreso de Debian y del Software Libre, ayudando a fortalecer la 
comunidad que continúa con los proyectos Debian durante el resto del 
año.

¡Muchas gracias, Hewlett-Packard por el apoyo a la DebConf15!

## ¡Transfórmate en patrocinador también!

DebConf15 está aceptando patrocinadores. La empresas y organizaciones 
interesadas pueden contactar con el equipo DebConf 
[sponsors@debconf.org](mailto:sponsors@debconf.org) y visitar el sitio web
de la DebConf15 en [http://debconf15.debconf.org](http://debconf15.debconf.org).

