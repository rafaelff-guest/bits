Title: New Debian Developers and Maintainers (September and October 2015)
Slug: new-developers-2015-10
Date: 2015-11-11 22:35
Author: Jean-Pierre Giraud
Tags: project
Status: published

The following contributors got their Debian Developer accounts in the last two months:

  * ChangZhuo Chen (czchen)
  * Eugene Zhukov (eugene)
  * Hugo Lefeuvre (hle)
  * Milan Kupcevic (milan)
  * Timo Weingärtner (tiwe)
  * Uwe Kleine-König (ukleinek)
  * Bernhard Schmidt (berni)
  * Stein Magnus Jodal (jodal)
  * Prach Pongpanich (prach)
  * Markus Koschany (apo)
  * Andy Simpkins (rattustrattus)

The following contributors were added as Debian Maintainers in the last two months:

  * Miguel A. Colón Vélez
  * Afif Elghraoui
  * Bastien Roucariès
  * Carsten Schoenert
  * Tomasz Nitecki
  * Christoph Ulrich Scholler
  * Mechtilde Stehmann
  * Alexandre Viau
  * Daniele Tricoli
  * Russell Sim
  * Benda Xu
  * Andrew Kelley
  * Ivan Udovichenko
  * Shih-Yuan Lee
  * Edward Betts
  * Punit Agrawal
  * Andreas Boll
  * Dave Hibberd
  * Alexandre Detiste
  * Marcio de Souza Oliveira
  * Andrew Ayer
  * Alf Gaida


Congratulations!
