Random ideas for future blogposts.

* "N easy ways to start contributing in Debian": 
a list of easy tasks to help debian, maybe with also an
indication of time required to perform them.
1. translating description of packages via DDTSS
   (http://ddtp.debian.org/ddtss/index.cgi/xx) [easy, quick, some
experience on translating]
2. report spam from Debian ML archive [easy, quick, no particular skills
required] (http://www.perrier.eu.org/weblog/2010/11/07)
3. bugs tagged gift on bts
(http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-qa@lists.debian.org;tag=gift)
## in theory this would be a good idea, but gift tag isn't used much in
debian bts, and YMMV on difficulty of the bug itself
[...]

Even better on a recruiting pov would be ask some particularly friendly
team (Perl?...?) to publish via bits.d.o easy tasks for newcomer (let's
say once a month).

* "Debian around the world": brief interviews to DUG from all over the world.

* "Debian On ...": a serie about the various hardware on which Debian runs or more in general about projects using Debian
 (and asking the readers to tell us if they know about Debian on some
specific piece of hardware or interesting project)
